<?php

namespace App\DataFixtures;


use App\Entity\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StudentMateuszFixtures extends Fixture
{
           
    const STUDENT_REFERENCE = 'student_mateusz';
    
    public function load(ObjectManager $manager) {
   
            $Student = new Student();
            $Student->setName('Mateusz');
            $Student->setSurname('Jankowski');
            
            $manager->persist($Student);
            $manager->flush();
            $this->addReference(self::STUDENT_REFERENCE, $Student);
            
    } 
    

}
