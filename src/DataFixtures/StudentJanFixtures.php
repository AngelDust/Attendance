<?php

namespace App\DataFixtures;


use App\Entity\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StudentJanFixtures extends Fixture
{
           
    const STUDENT_REFERENCE = 'student_jan';
    
    public function load(ObjectManager $manager) {
   
            $Student = new Student();
            $Student->setName('Jan');
            $Student->setSurname('Kowalski');
            
            $manager->persist($Student);
            $manager->flush();
            $this->addReference(self::STUDENT_REFERENCE, $Student);
            
    } 
    

}
