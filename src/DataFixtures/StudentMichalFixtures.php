<?php

namespace App\DataFixtures;


use App\Entity\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StudentMichalFixtures extends Fixture
{
           
    const STUDENT_REFERENCE = 'student_michal';
    
    public function load(ObjectManager $manager) {
   
            $Student = new Student();
            $Student->setName('Michal');
            $Student->setSurname('Ostowski');
            
            $manager->persist($Student);
            $manager->flush();
            $this->addReference(self::STUDENT_REFERENCE, $Student);
            
    } 
    

}
