<?php

namespace App\DataFixtures;


use App\Entity\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StudentDominikaFixtures extends Fixture
{
           
    const STUDENT_REFERENCE = 'student_dominika';
    
    public function load(ObjectManager $manager) {
   
            $Student = new Student();
            $Student->setName('Dominika');
            $Student->setSurname('Sulicka');
            
            $manager->persist($Student);
            $manager->flush();
            $this->addReference(self::STUDENT_REFERENCE, $Student);
            
    } 
    

}
