<?php

namespace App\DataFixtures;


use App\Entity\Attendance;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\DataFixtures\StudentDominikaFixtures;
use App\DataFixtures\StudentJanFixtures;
use App\DataFixtures\StudentMichalFixtures;
use App\DataFixtures\StudentKamilaFixtures;
use App\DataFixtures\StudentMateuszFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class AttendanceFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager) {
        $studentsListReference = [
            StudentDominikaFixtures::STUDENT_REFERENCE,
            StudentJanFixtures::STUDENT_REFERENCE,
            StudentKamilaFixtures::STUDENT_REFERENCE,
            StudentMateuszFixtures::STUDENT_REFERENCE,
            StudentMichalFixtures::STUDENT_REFERENCE
        ];

        for($a = 0;$a < count($studentsListReference); $a++){

            for ($i = 1; $i < 31; $i++) {
                $statusArray = array(' ', 'S', 'O', 'N');
                $roundStatus = array_rand($statusArray);
                $attendanceList[$a][] = array(
                    "status" => $statusArray[$roundStatus],
                    "data" => \DateTime::createFromFormat("Y-m-d", "2018-01-$i")
                );

            }
            for ($i = 1; $i < 28; $i++) {
                $statusArray = array(' ', 'S', 'O', 'N');
                $roundStatus = array_rand($statusArray);
                $attendanceList[$a][]= array(
                    "status" => $statusArray[$roundStatus],
                    "data" => \DateTime::createFromFormat("Y-m-d", "2018-02-$i")
                );

            }
            for ($i = 1; $i < 31; $i++) {
                $statusArray = array(' ', 'S', 'O', 'N');
                $roundStatus = array_rand($statusArray);
                $attendanceList[$a][]= array(
                    "status" => $statusArray[$roundStatus],
                    "data" => \DateTime::createFromFormat("Y-m-d", "2018-03-$i")
                );

            }
            for ($i = 1; $i < 30; $i++) {
                $statusArray = array(' ', 'S', 'O', 'N');
                $roundStatus = array_rand($statusArray);
                $attendanceList[$a][]= array(
                    "status" => $statusArray[$roundStatus],
                    'data' => \DateTime::createFromFormat("Y-m-d", "2018-04-$i")
                );
            }
            for ($i = 1; $i < 31; $i++) {
                $statusArray = array(' ', 'S', 'O', 'N');
                $roundStatus = array_rand($statusArray);
                $attendanceList[$a][] = array(
                    "status" => $statusArray[$roundStatus],
                    'data' => \DateTime::createFromFormat("Y-m-d", "2018-05-$i")
                );
            }
            for ($i = 1; $i < 31; $i++) {
                $statusArray = array(' ', 'S', 'O', 'N');
                $roundStatus = array_rand($statusArray);
                $attendanceList[$a][]= array(
                    "status" => $statusArray[$roundStatus],
                    "data" => \DateTime::createFromFormat("Y-m-d", "2018-06-$i")
                );
            }


            for ($i = 1; $i < 20; $i++) {
                $statusArray = array(' ', 'S', 'O', 'N');
                $roundStatus = array_rand($statusArray);
                $attendanceList[$a][]= array(
                    "status" => $statusArray[$roundStatus],
                    "data" => \DateTime::createFromFormat("Y-m-d", "2018-07-$i")
                );
            }



                foreach ($attendanceList[$a] as $attendanceDetails) {
                    $Attendance = new Attendance();
                    $Attendance->setStudent($this->getReference($studentsListReference[$a]));
                    $Attendance->setStatus($attendanceDetails['status']);
                    $Attendance->setDate($attendanceDetails['data']);
                    $manager->persist($Attendance);
                    $manager->flush();
                }

        }



    } 
     public function getDependencies()
    {
        return array(
            StudentDominikaFixtures::class,
            StudentJanFixtures::class,
            StudentKamilaFixtures::class,
            StudentMateuszFixtures::class,
            StudentMichalFixtures::class,

        );
    }

}
