<?php

namespace App\DataFixtures;


use App\Entity\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StudentKamilaFixtures extends Fixture
{
           
    const STUDENT_REFERENCE = 'student_kamila';
    
    public function load(ObjectManager $manager) {
   
            $Student = new Student();
            $Student->setName('Kamila');
            $Student->setSurname('Dąbrowska');
            
            $manager->persist($Student);
            $manager->flush();
            $this->addReference(self::STUDENT_REFERENCE, $Student);
            
    } 
    

}
