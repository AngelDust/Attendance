<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Student;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttendanceRepository")
 */
class Attendance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

     /**
     *@ORM\ManyToOne(targetEntity="Student", inversedBy="attendance", cascade={"persist"})
     *@ORM\JoinColumn(name="student", referencedColumnName="id")
     * 
     */
    private $student;

     /**
     * @ORM\Column(type="string", columnDefinition="enum(' ','O','N','S')")
     * 
     */
    private $status;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    public function getId()
    {
        return $this->id;
    }

    public function getStudent()
    {
        return $this->student;
    }
    
    public function setStudent(Student $student)
    {
        $this->student = $student;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(string $status)
    {
        $this->status = $status;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }
}
