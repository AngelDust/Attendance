<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Attendance;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StudentRepository")
 */
class Student
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

     /**
     * @ORM\Column(type="string", length=50)
     */
    private $surname;
    
     /**
     * @ORM\OneToMany(targetEntity="Attendance", mappedBy="student")
     */
    private $attendance;
    

    function getAttendance() {
        return $this->attendance;
    }

    function setAttendance($attendance) {
        $this->attendance = $attendance;
    }

        
    function getId(): int {
        return $this->id;
    }

    function getName(): string {
        return $this->name;
    }

    function getSurname(): string {
        return $this->surname;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setSurname($surname) {
        $this->surname = $surname;
    }


}
