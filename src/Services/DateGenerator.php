<?php

namespace App\Services;

class DateGenerator {
    
    public function renderDate (int $year , int $month, string $format = Null): array{
        $date = \DateTime::createFromFormat("Y-n", "$year-$month");
        $datesArray = array();
            for($i = 1; $i <= $date->format("t"); $i++){
                $datesArray[] = \DateTime::createFromFormat("Y-n-d", "$year-$month-$i")->format($format);
            }
        return $datesArray;
    }
    
    public function getDaysName(string $format): array{
        
        return $this->renderDate($format);
    }
    
    public function getMonthsName(string $format): array{
         return $this->renderDate($format);
    }
}
