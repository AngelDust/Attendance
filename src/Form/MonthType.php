<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class MonthType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('month', ChoiceType::class,
                    array(
                        'data' => date('n'),
                        'choices' => [
                            'Jan' => 1,
                            'Feb' => 2,
                            'Mar' => 3,
                            'Apr' => 4,
                            'May' => 5,
                            'Jun' => 6,
                            'Jul' => 7,
                            'Aug' => 8,
                            'Sep' => 9,
                            'Oct' => 10,
                            'Nov' => 11,
                            'Dec' => 12,
                        ],
                        'label' => 'month'                        
                    ))
            ->add('select', SubmitType::class, array('label' => 'select'))

        ;
    }
}