<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;




class AttendanceType extends AbstractType 
{
     public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('status', ChoiceType::class,
                    array(
                        'choices' => ["Pusty" => ' ',
                                      "Obecy" => 'O', 
                                      "Nieobecy" => 'N',
                                      "Spoźniony" => 'S'],
                        'label' => 'status'                        
                    
                     ))
            ->add('date', DateType::class);
    }
}
