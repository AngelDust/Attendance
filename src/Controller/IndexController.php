<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Form\MonthType;
use App\Entity\Attendance;
use App\Services\DateGenerator;
use App\Entity\Student;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index_page")
     */
    public function index(Request $request, DateGenerator $dateGenerator)
    {
        $slug = date('n');
        $form = $this->createForm(MonthType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $slug = $form['month']->getData();
        }
        
        $mount = $this->slug = $slug;
        $attendance = $this->getDoctrine()
                ->getRepository(Student::class)
                ->findAttendance();
        
        $choiceMount = $dateGenerator->renderDate(date('Y'),$mount,'d');
        $dayName = $dateGenerator->renderDate(date('Y'),$mount,'D');
        $dayMount = $dateGenerator->renderDate(date('Y'),$mount,'m');
        
        return $this->render('index/index.html.twig', [
            'choice_mounts' => $choiceMount,
            'daysAWeek' => $dayName,
            'monthInNumers' => $dayMount,
            'attendance' => $attendance,
            'slug' => $mount,
            'select' => $form->createView(),
 
        ]);
    }
    
     /**
     * @Route("/{id}", 
      * name="update", 
      * methods="GET|POST", 
      * options={"expose"=true}, 
      * requirements={"id"="\d+"})
     */
    public function edit(Request $request,Attendance $attendance, int $id)
    {
        
        if($request->isXmlHttpRequest()){
            $entytyManager = $this->getDoctrine()->getManager();
            $attendance = $entytyManager->getRepository(Attendance::class)->find($id);
            $attendance->setStatus($request->request->get('status'));
            $entytyManager->persist($attendance);
            $entytyManager->flush();
            $request->getSession()->getFlashBag()->add('success', 'Status studenta został zaktualizowany');
        }
 
        return new JsonResponse("Only Ajax", 200);
        
    }
     /**
     * @Route("/add", name="create", methods="GET|POST", options={"expose"=true})
     */
    public function create(Request $request)
    {
         if($request->isXmlHttpRequest()){
               $student = $this->getDoctrine()
                    ->getRepository(Student::class)
                    ->find($request->request->get('student'));
               
             $entytyManager = $this->getDoctrine()->getManager();
             $attendance = new Attendance();
             $attendance->setDate(\DateTime::createFromFormat('Y-m-d', $request->request->get('data')));
             $attendance->setStudent($student);
             $attendance->setStatus($request->request->get('status'));
             $entytyManager->persist($attendance);
             $entytyManager->flush();
             $request->getSession()->getFlashBag()->add('success', 'Status został dodany na dzień ' .$request->request->get('data'));
        }
    return new JsonResponse("Only Ajax", 200);
    }

    


}