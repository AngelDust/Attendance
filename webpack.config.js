var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    // the public path you will use in Symfony's asset() function - e.g. asset('build/some_file.js')
    .setManifestKeyPrefix('build/')


    .addEntry('css/app', './assets/css/global.scss')
    .addEntry('js/app', './assets/js/app.js')
    .autoProvidejQuery()
    .enableSassLoader()

;

module.exports = Encore.getWebpackConfig();
