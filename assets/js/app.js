const $ = require('jquery')
import 'bootstrap'
const Routes = require('../../public/js/fos_js_routes.json')
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router'
Routing.setRoutingData(Routes)

$('#updateFormModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var dateId = button.data('id')
  var dateStatus = button.data('status')
  var attendanceDate = button.data('date')
  var studentName = button.data('name')
  var modal = $(this)
  modal.find('#att_id').attr('data-update',dateId)
  modal.find('.modal-title').text(studentName)
  modal.find('#attendance_status').val(dateStatus)
  modal.find('#data-current').html(" Status na dzień: "  + attendanceDate)

})

$('#createFormModal').on('show.bs.modal', function (event) {
 var button = $(event.relatedTarget)
 var createStudentId = button.parent().attr('student-col-id')
 var createDate = button.parent().attr('data-select')
 var modal = $(this)
 modal.find('#createStudent').attr('data-student', createStudentId)
 modal.find('#createDate').html(createDate)


})

$(document).ready(function(){
$('table td').each(function(){
    let cell = $.trim($(this).text());
  if (cell.length == 0) {
            $(this).append("<button class='btn btn-success' data-toggle='modal' data-target='#createFormModal'>+</button>")
        }
 })

});



$('form#update_attendance_form').on('submit', function(e){
         e.preventDefault();
          let attendance_id = $('#att_id').data('update');

        var that = $(this),
        url = Routing.generate('update',{id: attendance_id}),
        data = {};
  
    that.find('[name]').each(function(){
       var that = $(this);
       var name = that.attr('name');
       var value = that.val();

       data[name] = value;
    });
    $.ajax({
        dataType: "json",
        type:'POST',
        url:url,
        data:data,
        success:function(){
             location.reload(); 
   
             
        }
 
    });
});

$('form#create_attendance_form').on('submit', function(e){
        e.preventDefault();
        
        var id =  $('#createStudent').data('student')
        console.log(id)
        var date = $('#createDate').html()
        var status = $('#attendance_status_create').val() 
  
    $.ajax({
        dataType: "json",
        type:'POST',
        url:'/add',
        data:{
            student: id,
            status: status,
            data: date
        },
        success:function(){
          location.reload(); 
   
             
        }
 
    });
});